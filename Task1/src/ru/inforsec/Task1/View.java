package ru.inforsec.Task1;
	 
import java.util.Scanner;
import java.io.*;

public class View
{
	public int[] input(int[] a)
	{
		Scanner in = new Scanner(System.in);
		
		System.out.print("Enter array-capacity: ");
		
		try {
			int size = in.nextInt();
			a = new int[size];
			System.out.println("Enter array elements: ");
			for (int i = 0; i < size; i++)
				a[i] = in.nextInt();
		} catch (NullPointerException e) { 
              System.out.println("Error!"); 
        } 
		finally {
			return a;
		}
	}
	
	public void out(int[] a)
	{
		System.out.print("The result: ");
		for (int i = 0; i < a.length; i++)
			System.out.print(" " + a[i]);
		System.out.println("");
	}
}